"""Module for constants"""
from rdflib import Namespace

# The PHT namespace:
pht_namespace = Namespace("https://github.com/LaurenzNeumann/PHTMetadata#")
